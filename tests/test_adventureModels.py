import pytest
from app.adventures import *
from app.adventures.models import AdLocation, AdLocationDescription, AdAdventure, AdDescriptionFlags, AdActionFlags, AdAction


def test_Adventure_Create(session):
    location = test_location_create(session)
    adventure = AdAdventure(
        description='Test description of adventure', locations=location.id)
    session.add(adventure)
    session.commit()
    assert adventure.id > 0
    return adventure


def test_Description_Flag_Create(session):
    flag = AdDescriptionFlags()
    flag.active = True
    flag.name = 'TestDescFlag'
    flag.flag_description = 'Test flag'
    session.add(flag)
    session.commit()
    assert flag.id > 0
    return flag


def test_location_create(session):
    # Adventure(description='Test description of adventure 2')
    #adventure = test_Adventure_Create(session)
    flag = test_Description_Flag_Create(session)
    # session.add(adventure)
    location = AdLocation(name='Test location', desc_flags=flag.id)
    session.add(location)
    session.commit()
    assert location.id > 0
    return location


def test_Description_Create(session):
    location = test_location_create(session)
    description = AdLocationDescription()
    description.description = 'Test location description'
    flag = AdDescriptionFlags()
    flag.active = True
    flag.name = 'TestDescFlag'
    flag.flag_description = 'Test flag'
    session.add(flag)
    session.commit()
    flag = AdDescriptionFlags.query.filter_by(id=flag.id).first()
    print(flag)
    description.flag_id == flag.id
    description.location = location.id
    session.add(description)
    session.commit()
    assert description.id > 0


def test_Select_location_without_description(session):
    test_location_create(session)
    location = AdLocation.query.filter_by(description=None).count()
    assert location == 1


def test_Select_location_with_description(session):
    location = test_location_create(session)
    description = AdLocationDescription()
    description.description = 'Test location description'
    flag = AdDescriptionFlags()
    flag.active = True
    flag.name = 'TestDescFlag'
    flag.flag_description = 'Test flag'
    session.add(flag)
    session.commit()
    flag = AdDescriptionFlags.query.filter_by(id=flag.id).first()
    print(flag)
    description.flag_id == flag.id
    description.location = location.id
    session.add(description)
    session.commit()
    count = AdLocation.query.filter(AdLocation.description != None).count()
    assert count > 0


def test_addAction(session):
    action = AdAction()
    action.description = 'Stay here'
    action.location = test_location_create(session).id
    action.name = 'Stay'
    action.flag_id = test_addFlag(session).id
    session.add(action)
    session.commit()
    assert action.id > 0


def test_addFlag(session):
    flag = AdActionFlags()
    flag.name = 'Stay here flag'
    flag.active = True
    flag.flag_description = 'Flag for stay here'
    session.add(flag)
    session.commit()
    assert flag.id > 0
    return flag


def test_addDescriptionFlag(session):
    flag = AdDescriptionFlags()
    flag.name = 'New descr flag'
    flag.active = True
    flag.flag_description = 'Flag for stay here'
    session.add(flag)
    session.commit()
    assert flag.id > 0
    return flag


# def test1_addFlag(AdLocation, Object):
#     assert type(Object) == AdAction
