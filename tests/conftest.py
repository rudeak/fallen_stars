import os
import pytest
from dotenv import load_dotenv


from app import create_app
from app import db as _db
from app import adventures
from app.adventures.models import *
from config import TestConfig


basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))
TESTDB = 'test_project.db'
TESTDB_PATH = os.environ.get('DATABASE_URL') or \
    'sqlite:///' + os.path.join(basedir, TESTDB)


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    print(TestConfig.SQLALCHEMY_DATABASE_URI)
    app = create_app(TestConfig)

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope='session')
def db(app, request):
    """Session-wide test database."""
    if os.path.exists(TESTDB_PATH):
        os.unlink(TESTDB_PATH)

    def teardown():
        with app.app_context():
            pass
            _db.drop_all()
    with app.app_context():
        _db.app = app
        _db.create_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test."""
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session
