from flask import Blueprint, current_app
from app.adventures import models
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os


bpAdventures = Blueprint('adventures', __name__)

bpLoger = logging.getLogger(__name__)

if not os.path.exists('logs'):
    os.mkdir('logs')
file_handler = RotatingFileHandler('logs/adventures.log',
                                   maxBytes=10240, backupCount=10)
file_handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'))
file_handler.setLevel(logging.INFO)
bpLoger.addHandler(file_handler)
bpLoger.setLevel(logging.INFO)
bpLoger.info('Adventures blueprint started')
