from app import db


class AdDescriptionFlags (db.Model):
    """
        Клас флагов для описаний локаций
        Поля:
            * id - Primary Key, Integer
            * active - признак активности флага, Boolean, default = False
            * name - название флага, String(20)
            * flag_description - описание флага, String(20)
            * location - связь с локацией
            * description - связь с описанием которому принадлежит этот флаг
            * reverse_result - связь с результатом действия которое изменяет даный флаг
    """
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(20), nullable=False)
    flag_description = db.Column(db.String(20))
    location = db.relationship(
        'AdLocation', backref='location_description', lazy='dynamic')
    description = db.relationship(
        'AdLocationDescription',
        uselist=False,
        back_populates='flag')
    reverse_result = db.relationship(
        'AdActionResult', backref='result_reverse', lazy='dynamic')


class AdActionFlags (db.Model):
    """
        Клас флагов для описаний действий
        Поля:
            * id - Primary Key, Integer
            * active - признак активности флага, Boolean, default = False
            * name - название флага, String(20)
            * flag_description - описание флага, String(20)
            * action - связь с локацией на которой происходит изменение флага
            * reverse_result - связь с действием которое изменяет состояние флага
    """
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(20), nullable=False)
    flag_description = db.Column(db.String(20))
    location = db.relationship(
        'AdLocation', backref='action_location', lazy='dynamic')
    action = db.relationship(
        'AdAction',
        uselist=False,
        backref='ad_action_flag')
    reverse_result = db.relationship(
        'AdActionResult', backref='action_result', lazy='dynamic')


class AdAction (db.Model):
    """
        Клас описания действий
        Поля:
            * id - Primary Key, Integer
            * description - описание локации String(255), unique=True
            * location - внешний ключ - ссылка на Location.id, Integer (таблица локаций)
            * flag_id - внешний ключ - ссылка на ActionFlagss.id, Integer
            *            (таблица флагов описания, one-to-one relationship)
            * flag - связь db.relationship("ActionFlags", back_populates="description")
            *            ccылка на родительский флаг для one-to-one relationship
            * action_result - связь с таблицей результатов действия:
            *    - изменеие состояния флага
            *    - переход на другую локацию
            *    - изменение описания локации
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    description = db.Column(db.String(255), unique=True)
    location = db.Column(db.Integer, db.ForeignKey('ad_location.id'))
    flag_id = db.Column(db.Integer, db.ForeignKey('ad_action_flags.id'))
    flag = db.relationship("AdActionFlags",  foreign_keys=[flag_id])
    action_result = db.relationship(
        'AdActionResult', backref='result', lazy='dynamic')


class AdActionResult(db.Model):
    """
        Клас результатов действий
        Поля:
            * id - Primary Key, Integer
            * action - Внешний ключ, ссылка на действие
            * goto_location - ссылка на переход на новою локацию
            * reverse_description - ссылка на изменение флага описания локации
            * reverse_action - ссылка нп изменение флага видимости действий локации
    """
    id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.Integer, db.ForeignKey('ad_action.id'))
    goto_location = db.Column(db.Integer, db.ForeignKey('ad_location.id'))
    reverse_description = db.Column(
        db.Integer, db.ForeignKey('ad_description_flags.id'))
    reverse_action = db.Column(
        db.Integer, db.ForeignKey('ad_action_flags.id'))


class AdLocation(db.Model):
    """
        Клас локаций для описаний объектов локации
        Поля:
            * id - Primary Key, Integer
            * name - название локации String(20)
            * adventure - внешний ключ - ссылка на Adventure.id, Integer (таблица квестов)
            * description - Ссылка на описания локаций
            * desc_flags - внешний ключ - ссылка на LocationDescription.id, Integer
            *         (таблица вариантов описания локаций)
            * action - список действий локации
            * action_flags - ссылка на флаги действий которые могут изменятся на этой локации
            * goto - связь с действием, которое приведет к этой локации
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    adventure = db.relationship(
        'AdAdventure', backref='my_adventure', lazy='dynamic')
    description = db.relationship(
        'AdLocationDescription', backref='location_description', lazy='dynamic')
    action = db.relationship(
        'AdAction', backref='actions', lazy='dynamic')
    desc_flags = db.Column(
        db.Integer, db.ForeignKey('ad_description_flags.id'))
    action_flags = db.Column(db.Integer, db.ForeignKey('ad_action_flags.id'))
    goto = db.relationship(
        'AdActionResult', backref='came_from', lazy='dynamic')


class AdLocationDescription (db.Model):
    """
        Клас описания локаций
        Поля:
            * id - Primary Key, Integer
            * description - описание локации String(255), unique=True
            * location - внешний ключ - ссылка на Location.id, Integer (таблица локаций)
            * flag_id - внешний ключ - ссылка на DescriptionFlags.id, Integer
            *            (таблица флагов описания, one-to-one relationship)
            * flag - db.relationship("DescriptionFlags", back_populates="description")
            *            ccылка на родительский флаг для one-to-one relationship
    """
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255), unique=True)
    location = db.Column(db.Integer, db.ForeignKey('ad_location.id'))
    flag_id = db.Column(db.Integer, db.ForeignKey('ad_description_flags.id'))
    flag = db.relationship("AdDescriptionFlags", back_populates="description")


class AdAdventure (db.Model):
    """ Description
        Общая модель текстового квеста содержит следующие поля:
        * id - главный ключ, Integer
        * description - Описание квеста,  String
        * locations - связь с локациями квеста
    """
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255), unique=True, nullable=False)
    locations = db.Column(db.Integer, db.ForeignKey('ad_location.id'))
