-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Час створення: Бер 12 2019 р., 09:41
-- Версія сервера: 10.3.13-MariaDB
-- Версія PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `fs_db`
--

-- --------------------------------------------------------

--
-- Структура таблиці `action`
--

CREATE TABLE `action` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL,
  `goto` int(11) DEFAULT NULL,
  `revert_flag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `action_flags`
--

CREATE TABLE `action_flags` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `flag_description` varchar(20) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Структура таблиці `adventure`
--

CREATE TABLE `adventure` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `alembic_version`
--

CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `alembic_version`
--

INSERT INTO `alembic_version` (`version_num`) VALUES
('2a98dfb315d9');

-- --------------------------------------------------------

--
-- Структура таблиці `description_flags`
--

CREATE TABLE `description_flags` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `flag_description` varchar(20) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Структура таблиці `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `adventure` int(11) DEFAULT NULL,
  `flags` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблиці `location_description`
--

CREATE TABLE `location_description` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `flag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`),
  ADD KEY `flag_id` (`flag_id`),
  ADD KEY `goto` (`goto`),
  ADD KEY `location` (`location`),
  ADD KEY `revert_flag_id` (`revert_flag_id`);

--
-- Індекси таблиці `action_flags`
--
ALTER TABLE `action_flags`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `adventure`
--
ALTER TABLE `adventure`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Індекси таблиці `alembic_version`
--
ALTER TABLE `alembic_version`
  ADD PRIMARY KEY (`version_num`);

--
-- Індекси таблиці `description_flags`
--
ALTER TABLE `description_flags`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adventure` (`adventure`),
  ADD KEY `flags` (`flags`);

--
-- Індекси таблиці `location_description`
--
ALTER TABLE `location_description`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description` (`description`),
  ADD KEY `flag_id` (`flag_id`),
  ADD KEY `location` (`location`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `action`
--
ALTER TABLE `action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `action_flags`
--
ALTER TABLE `action_flags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `adventure`
--
ALTER TABLE `adventure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `description_flags`
--
ALTER TABLE `description_flags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `location_description`
--
ALTER TABLE `location_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `action`
--
ALTER TABLE `action`
  ADD CONSTRAINT `action_ibfk_1` FOREIGN KEY (`flag_id`) REFERENCES `action_flags` (`id`),
  ADD CONSTRAINT `action_ibfk_2` FOREIGN KEY (`goto`) REFERENCES `location` (`id`),
  ADD CONSTRAINT `action_ibfk_3` FOREIGN KEY (`location`) REFERENCES `location` (`id`),
  ADD CONSTRAINT `action_ibfk_4` FOREIGN KEY (`revert_flag_id`) REFERENCES `action_flags` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `location_ibfk_1` FOREIGN KEY (`adventure`) REFERENCES `adventure` (`id`),
  ADD CONSTRAINT `location_ibfk_2` FOREIGN KEY (`flags`) REFERENCES `description_flags` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `location_description`
--
ALTER TABLE `location_description`
  ADD CONSTRAINT `location_description_ibfk_1` FOREIGN KEY (`flag_id`) REFERENCES `description_flags` (`id`),
  ADD CONSTRAINT `location_description_ibfk_2` FOREIGN KEY (`location`) REFERENCES `location` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
